<?php
    set_time_limit(30);
    require($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/header.php' );
    $APPLICATION->SetTitle('Активность в чатах');
    $filterName = 'FILTER_MESSAGES';
    $gridName = 'GRID_MESSAGES';
    $list = [];
    $filter = [];
    $filterHeader = [
        'FILTER_ID' => $filterName,
        'GRID_ID' => $gridName,
        'FILTER' => [
            ['id' => 'DATE', 'name' => 'Дата', 'type' => 'date', 'default' => true],
            [
                'id' => 'USER_ID',
                'name' => 'Ответственный',
                'type' => 'dest_selector',
                'default' => true,
                'params' => [
                    'useNewCallback' => 'Y',
                    //'lazyLoad' => 'Y',
                    //'context' => $propName,
                    'contextCode' => 'U',
                    'enableUsers' => 'Y',
                    'userSearchArea' => 'I', // E
                    'enableSonetgroups' => 'N',
                    'enableDepartments' => 'N',
                    'allowAddSocNetGroup' => 'N',
                    'departmentSelectDisable' => 'Y',
                    'showVacations' => 'Y',
                    'duplicate' => 'N',
                    'enableAll' => 'N',
                    'departmentFlatEnable' => 'N',
                    'useSearch' => 'Y',
                    'multiple' => 'Y',
                    'enableEmpty' => 'N'
                ]
            ],
        ],
        'ENABLE_LIVE_SEARCH' => 'N',
        'ENABLE_LABEL' => 'N',
        'THEME' => 'BORDER'
    ];

    \Bitrix\UI\Toolbar\Facade\Toolbar::addFilter($filterHeader);
    // Получаем данные для фильтрации.
    $filterOptions = new \Bitrix\Main\UI\Filter\Options( $filterName );
    $filterFields = $filterOptions->getFilter();

    if ( $filterFields['USER_ID'] )
    {
        foreach ( $filterFields['USER_ID'] as $key => $userId )
        {
            $filterFields['USER_ID'][$key] = str_ireplace('U', '', $userId);
        }
    }

    $mustNotifyModule = ['im'];
    $mustEvents = ['private', 'tasks'];
    $filter = [
        'NOTIFY_MODULE' => $mustNotifyModule,
        'NOTIFY_EVENT' => $mustEvents,
    ];

    if ( $filterFields['DATE_from'] )
    {
        $filter['>=DATE_CREATE'] = $filterFields['DATE_from'];
    }
    if ( $filterFields['DATE_to'] )
    {
        $filter['<=DATE_CREATE'] = $filterFields['DATE_to'];
    }

    foreach ( $filterFields['USER_ID'] as $key => $userId )
    {
        $filter['AUTHOR_ID'] = $userId;

        $filter['NOTIFY_MODULE'] = 'im';
        $filter['NOTIFY_EVENT'] = 'private';
        $privateCount = \Bitrix\Im\MessageTable::getList([
            'filter' => $filter,
            'count_total' => true
        ])->getCount();

        $filter['NOTIFY_EVENT'] = 'group';
        $groupCount = \Bitrix\Im\MessageTable::getList([
            'filter' => $filter,
            'count_total' => true
        ])->getCount();

        $filter['NOTIFY_MODULE'] = 'tasks';
        $filter['NOTIFY_EVENT'] = 'comment';
        $tasksCount = \Bitrix\Im\MessageTable::getList([
            'filter' => $filter,
            'count_total' => true
        ])->getCount();

        /*$filter['NOTIFY_MODULE'] = 'blog';
        $filter['NOTIFY_EVENT'] = 'comment';
        $blogCount = \Bitrix\Im\MessageTable::getList([
            'filter' => $filter,
            'count_total' => true,
        ])->getCount();
        */

        unset($filter['NOTIFY_MODULE'],$filter['NOTIFY_EVENT']);
        $chatCountCreated = \Bitrix\Im\Model\ChatTable::getList([
                'filter' => $filter,
                'count_total' => true
        ])->getCount();

        $inChat = CIMChat::GetChatData([
            'SKIP_PRIVATE' => 'N',
            'GET_LIST' => 'Y',
            'USER_ID' => $userId,
        ])['userInChat'];

        $list[] = [
            'data' => [
                'ID' =>   $key,
                'user' => $filterFields['USER_ID_label'][$key],
                'private' =>  $privateCount,
                'group' => $groupCount,
                'tasks' => $tasksCount,
                'chat' => $chatCountCreated,
                'in_chat' =>  count($inChat)

                //'blog' => $blogCount
            ]
        ];
    }

    $grid_options = new Bitrix\Main\Grid\Options($filterName);

    $sort = $grid_options->GetSorting(['sort' => ['ID' => 'ASC'], 'vars'=>array('by'=>'by', 'order'=>'order')  ]);
    $nav_params = $grid_options->GetNavParams();
    $nav = new Bitrix\Main\UI\PageNavigation($filterName);
    $nav->allowAllRecords(true)
        ->setPageSize($nav_params['nPageSize'])
        ->initFromUri();

    $APPLICATION->IncludeComponent(
        'bitrix:main.ui.grid',
        '',
        [
            'GRID_ID' => $gridName,
            'COLUMNS' => [
                ['id' => 'user', 'name' => 'Сотрудник', 'default' => true],
                ['id' => 'private', 'name' => 'Приватные чаты (сообщ)', 'default' => true],
                ['id' => 'group', 'name' => 'Групповые чаты (сообщ)', 'default' => true],
                ['id' => 'tasks', 'name' => 'В задачах (сообщ)', 'default' => true],
                ['id' => 'chat', 'name' => 'Создал чатов', 'default' => true],
                ['id' => 'in_chat', 'name' => 'Участник чатов (все время)', 'default' => true],
                //['id' => 'blog', 'name' => 'В ленте', 'default' => true],
            ],
            'ROWS' => $list,
            'NAV_OBJECT' => $nav,
            'AJAX_MODE' => 'Y',
            'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
            'PAGE_SIZES' => [
                ['NAME' => '5', 'VALUE' => '5'],
                ['NAME' => '10', 'VALUE' => '10'],
                ['NAME' => '25', 'VALUE' => '25'],
            ],
            'SHOW_ROW_CHECKBOXES' => false,
            'AJAX_OPTION_JUMP'          => 'N',
            'SHOW_CHECK_ALL_CHECKBOXES' => false,
            'SHOW_ROW_ACTIONS_MENU'     => false,
            'SHOW_GRID_SETTINGS_MENU'   => true,
            'SHOW_NAVIGATION_PANEL'     => true,
            'SHOW_PAGINATION'           => true,
            'SHOW_SELECTED_COUNTER'     => false,
            'SHOW_TOTAL_COUNTER'        => false,
            'SHOW_PAGESIZE'             => true,
            'SHOW_ACTION_PANEL'         => false,
            'ALLOW_COLUMNS_SORT'        => true,
            'ALLOW_COLUMNS_RESIZE'      => true,
            'ALLOW_HORIZONTAL_SCROLL'   => true,
            'ALLOW_SORT'                => true,
            'ALLOW_PIN_HEADER'          => true,
            'AJAX_OPTION_HISTORY'       => 'Y'
        ]
    );

    require($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/footer.php' );